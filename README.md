
<!-- README.md is generated from README.Rmd. Please edit that file -->

## applyPGS pipeline

This pipeline is designed to applied pre-built PGS to selected samples
of UKBB.

## Requirements

R versions \>= 3.4.0.

### To clone the applyPGS repository do:

``` bash
# clone the applyPGS repository
 git clone https://gitlab.com/ukbb_covid19/applypgs.git
 
```

### To install the R libraries required for the pipeline do:

``` r
## Within R:
install.packages( "http://www.well.ox.ac.uk/~gav/resources/rbgen_v1.1.4.tgz", repos = NULL, type = "source" )

install.packages("devtools") # if you don't already have the package

library(devtools)
devtools::install_gitlab("evigorito/runpgs")
```

# Workflow

The
[Snakefile](https://gitlab.com/ukbb_covid19/applypgs/-/blob/master/Snakefile)
details the steps performed for analysis. The config.yaml file is not
inlcuded in the repo because contains paths to data directories. For
each PGS score there is an output file with the scores for each
individual and if any SNP was not found on bgen file or any
inconsistencies were observed beteen bgen PGS source, those are reported
in a file with prefix “failed\_rsid\_”
