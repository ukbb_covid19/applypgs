
import os
import glob
import pandas as pd
from functools import reduce

def merge_pgs(files, out_file):
    """merge pgs files: merge files by all columns except score and add score column. files: list with partial PGS scores, out_file: file name to save total score. If "failed" files are in the dir of the PGS scores, rbind all files into one"""
    comb = None
    for f in files:
        df = pd.read_csv(f, sep = " ")
        # get index for col 'score'
        score_idx = df.columns.get_loc("score")
        # set index for df all cols except 'score'
        df.set_index(df.columns[[x for x in range(score_idx)]].tolist(), inplace=True)
        if comb is None:
            comb = df.copy()
        else:
            comb = reduce(lambda x, y: x.add(y), [comb, df])

    comb.to_csv(out_file, sep=" ", index=True, na_rep="NA")

    # look for "failed" files

    dir_failed = os.path.dirname(files[0])

    fails = glob.glob(dir_failed + "/failed*")

    if len(fails):
        all = None
        for f in fails:
            df = pd.read_csv(f, sep = "\t")
            if all is None:
                all = df.copy()
            else:
                all = all.append(df)            
        all.to_csv(dir_failed + "/" + os.path.basename(dir_failed) + "failed.all.txt", sep="\t")
        
     
files = snakemake.input['scores']
out_file = snakemake.output['scores']

merge_pgs(files, out_file)
