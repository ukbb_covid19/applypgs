library(runPGS) 

library(tictoc)
tic("Start running")

samples <- snakemake@input[['samples']]
sample_bgen <- snakemake@input[['sample_bgen']]
code <- snakemake@params[['code']]
build <- snakemake@params[['genome_build']]
chunk <- as.numeric(snakemake@params[['chunk']])
cols <- snakemake@params[['cols']]

if(!exists('samples')) samples <-NULL
if(!exists('sample_bgen')) sample_bgen <-NULL
if(!exists('code')) code <- NULL

if(is.null(cols)){
    cols <- c("chr_name",  "chr_position",  "reference_allele", "effect_allele","rsID", "effect_weight")

}


chrom=snakemake@wildcards[['chrom']]
end_line=as.numeric(snakemake@wildcards[['breaks']])

start_line <- ifelse(end_line %% chunk == 0, end_line - chunk + 1, end_line - (end_line %% chunk) + 1)

lines <- c(start_line, end_line)

print(lines)

pgs.ext(b.file=snakemake@input[['bgen']],
        chrom=chrom,
        pgs=snakemake@input[['pgs']],
        chr_col=cols[1],
        pos_col =cols[2],
        ref_col= cols[3],
        alt_col=cols[4],
        rs_col=cols[5],
        weight_col =cols[6],
        lines=lines,
        samples=samples,
        code=code,
        sample_bgen=sample_bgen,
        build=build,
        out=snakemake@output[['out']])

toc()

## testing: added to geno_pgs_efficiency.R
