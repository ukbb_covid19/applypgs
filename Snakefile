##############################################################
## Snakefile for applying PGS to UKBB data 
##############################################################

shell.prefix("source ~/.bashrc; ") ## easy access to tools and directories

configfile: "config.yaml" ## file with configuration parameters are read from [config](config.yaml). You can edit this file to specify the location of your own dirs and files.

localrules: all

import os
import re
import pandas as pd
import gzip


from snakemake.remote.FTP import RemoteProvider as FTPRemoteProvider
FTP = FTPRemoteProvider()

from snakemake.remote.HTTP import RemoteProvider as HTTPRemoteProvider
HTTP = HTTPRemoteProvider()


def pgs_id(f=config['pgs_meta_all']):
    """ Get PGS id for a trait, as coded in PGS catalogue"""
    data=pd.read_csv(f)
    # remove everthing between '<>'
    data.replace(to_replace='<.+>', value="", regex=True, inplace=True)
    data.columns = ["PGS_ID"] + [col for col in data.columns[1:]]
    return data


def dic_pd(col1="ukbb_phenotype", col2="ukbb_code", df=pgs_id()):
    """Makes a dictionary with keys col1 and values col2 from pandas data frame"""
    df=df.drop_duplicates(subset = [col1,col2])
    keys=df[col1].tolist()
    values=df[col2].tolist()
    dic=dict(zip(keys,values))
    return(dic)

def pgs_build(id=set(pgs_id()["PGS_ID"].tolist())):
    """Make a dictionary with keys a pgs id and value genome build.
    id=list of PGS scores to use"""
    dic={}
    keys=id
    for k in keys:
        with gzip.open(config['output_dir'] + "/inputPGS/" + k + ".txt.gz", "r") as f:
            for line in f:
                if b'Original Genome Build' in line:
                    dic[k] = line.decode("utf-8").strip('\n').split('= ')[1]
                    break
    return(dic)

def pgs_chrom_n(id=set(pgs_id()["PGS_ID"].tolist())):
    """Make a dictionary with keys PGS id and values a pandas series. Each series has keys chromosomes and values the number of variants for it"""
    dic={}
    keys=id
    for k in keys:
        df = pd.read_csv(config['output_dir'] + "/inputPGS/" + k + ".txt.gz", sep='\t', comment='#')        
        dic[k]=df['chr_name'].value_counts()
        # for chrom, group in chroms:
        #     v.append([chrom, len(group)])
        # dic[k] = v
    return(dic)

def pgsldpred2_chrom_n(id):
    """Make a dictionary with key a PGS ldpred2 file name  and values a pandas series. Each series has keys chromosomes and values the number of variants for it"""
    dic={}
    df = pd.read_csv(config['pgs_ldpred2'] + "/" + id , sep='\t')        
    dic[id]=df['CHR19'].value_counts()
    return(dic)


def break_chrom(d, b=1000):
    """given an element from a dictionary with key PGS name and value a pandas series with keys chromosome and values total number of variants per chromosome (created with pgs_chrom_n,create a list with the end line to read from PGS file for each job"""
    dic={}
    keys=d.keys()
    for key in keys:
        v=[x for x in range(b, d[key], b)]
        v.append(d[key])
        dic[key] = v
    return(dic)
                                      

# define variables to avoid repeating computation
ukbbDic=dic_pd()
pgs_ukbb=dic_pd(col1="PGS_ID", col2="ukbb_phenotype")
df=pd.read_csv(config['pgsldpred2ukbb'], sep=" ")
ld2_ukbbDic=dic_pd(df=df[df.ukbb_code != 0])
pgsld2_ukbb=dic_pd(col1="PGS_ID", col2="ukbb_phenotype", df=df[df.ukbb_code != 0])

ld2_code=[x for x in pgsld2_ukbb.keys()] # ld2pgs with phenotype in ukbb
ld2_nocode=df[~df.PGS_ID.isin(ld2_code)]['PGS_ID'].tolist() # ld2pgs w/o phenotype in ukbb


built=pgs_build()
n_chrom=pgs_chrom_n()
pgs2test= ['PGS000034','PGS000116']

ld2n_chrom=pgsldpred2_chrom_n("BMI_Locke_25673413_1_LDpred2_auto_1e4_4000_qcfilt.full.model")
ld2_breaks=break_chrom(d=ld2n_chrom["BMI_Locke_25673413_1_LDpred2_auto_1e4_4000_qcfilt.full.model"], b=1000)

ld2_dic_counts={s:pgsldpred2_chrom_n(s)[s] for s in pgsld2_ukbb.keys()}


rule all:
    input:
        # expand(config['output_dir'] + "/inputPGS/{score}.txt.gz", score=set(pgs_id()["PGS_ID"].tolist())),
        # config['output_dir'] + "/ukbb_PGS/eth1001_PGS000010.txt",
        # config['output_dir'] + "/inputPGS/metadata/processed/pgs_scores_all.csv",
        # expand(config['output_dir'] + "/input_bb/{ukbb_phenotype}.tab", ukbb_phenotype=ld2_ukbbDic.keys()),     
        # expand(config['output_dir'] + "/input_bb/{ukbb_phenotype}.tab", ukbb_phenotype=ukbbDic.keys()),
        # expand(config['output_dir'] + "/ukbb_PGS/{score}{chrom}.txt", score=["PGS000116"], chrom= breaks_pgs.keys())  #pgs_ukbb.keys())
        # [config['output_dir'] + "/ukbb_PGS/{score}/{score}.{chrom}.{breaks}.txt".format(score=s, chrom=str(bk), breaks=str(v)) for s in pgs2test for  bk in break_chrom(n_chrom[s]).keys() for v in break_chrom(n_chrom[s])[bk]],
        # expand(config['output_dir'] + "/ukbb_PGS/{score}/{score}.total.txt", score=pgs2test)
        # expand(config['output_dir'] + "/ukbb_PGS/{score}/{score}.{chrom}.{breaks}.txt", score=['PGS000116'], chrom=11, breaks=2453)
        # expand(config['output_dir'] + "/ukbb_ldpred2/{ldname}/{ldname}.{chrom}.{breaks}.txt",
        #        ldname=["BMI_Locke_25673413_1_LDpred2_auto_1e4_4000_qcfilt.full.model"],
        #        chrom=1,
        #        breaks=test)
        # [config['output_dir'] + "/ukbb_ldpred2/{ldname}/{ldname}.{chrom}.{breaks}.txt".format(
        #        ldname=s,
        #        chrom=str(bk),
        #        breaks=str(v)) for s in ["BMI_Locke_25673413_1_LDpred2_auto_1e4_4000_qcfilt.full.model"] for bk in ld2_breaks.keys() for v in ld2_breaks[bk]],
        # [config['output_dir'] + "/ukbb_ldpred2/{ldname}/{ldname}.{chrom}.{breaks}.txt".format(
        #        ldname=s,
        #        chrom=str(bk),
        #        breaks=str(v)) for s in pgsld2_ukbb.keys() for bk in break_chrom(d=pgsldpred2_chrom_n(s)[s],b= 1000).keys() for v in break_chrom(d=pgsldpred2_chrom_n(s)[s], b=1000)[bk]],
        # [config['output_dir'] + "/ukbb_ldpred2/{ldname}/{ldname}.{chrom}.{breaks}.txt".format(
        #        ldname=s,
        #        chrom=str(bk),
        #        breaks=str(v)) for s in pgsld2_ukbb.keys() for bk in break_chrom(d=ld2_dic_counts[s],b= 1000).keys() for v in break_chrom(d=ld2_dic_counts[s], b=1000)[bk]]
        # expand(config['output_dir'] + "/ukbb_ldpred2/{ldname}/{ldname}.total.txt", ldname=pgsld2_ukbb.keys())
        "Scripts/test.ldpred2_scores.pdf"
        
rule PGS_meta_merge:
    """Merge files downloaded from PGS catalogue for groups of traits. To get pipeline to work I need to start with an initial file (config['pgs_meta_all'] for function pgs_id to work. I scp  PGS files from local to os.path.dirname((config['pgs_meta_all']). Once merged into config['pgs_meta_all'] they move to "path to/metadata/processed" dir. I need an output file for the rule to work, but I have it in 'processed' dir  so I can ignore it when I add more PGS files to merge. When more files are added I need to re-run this rule as snakemake -R PGS_meta_merge """
    input:
        meta=config['pgs_meta_all'],
        ukbb=config['pgs2ukbb']
    output:
        config['output_dir'] + "/inputPGS/metadata/processed/pgs_scores_all.csv"
    run:
        direc=os.path.dirname(input.meta)
        files=[os.path.join(direc, f) for f in os.listdir(direc)  if re.match("(?!individual|processed)", f)]
        # check for new files, if they are, append, otherwise make output from latest merged file
        if len(files):
            dfs=[pgs_id(f) for f in files]
            df=pd.concat(dfs, sort=False)
            # merge with ukbb info
            ukbb=pd.read_csv(input.ukbb, sep=" ")
            df=pd.merge(df, ukbb, on="PGS Name")
            df.to_csv(output[0], index=False)
            # move 'files' to 'processed' dir
            newfiles=[f for f in files if re.match("(?!" + input[0] + ")",f)]
            for f in newfiles:
                os.rename(f, os.path.join(os.path.dirname(output[0]), os.path.basename(f)) )
            # update starting file
            shell("cp {output} {config[pgs_meta_all]} ")
        else:
            shell("cp {config[pgs_meta_all]} {output}")
        
rule PGS_down:
    """ Download PGS files"""
    input:
        score=FTP.remote("ftp.ebi.ac.uk/pub/databases/spot/pgs/scores/{score}/ScoringFiles/{score}.txt.gz",
                         keep_local=True, immediate_close=True),
        meta=FTP.remote("ftp.ebi.ac.uk/pub/databases/spot/pgs/scores/{score}/Metadata/{score}_metadata_efo_traits.csv", keep_local=True, immediate_close=True)
    output:
        score=config['output_dir'] + "/inputPGS/{score}.txt.gz",
        meta=config['output_dir'] + "/inputPGS/metadata/individual/{score}_metadata_efo_traits.csv" 
    shell:
        "mv {input.score} {output.score} ;"
        "mv {input.meta} {output.meta} "


# rule PGS_format:
#     """Add chromosome column and store as table for easy access"""
#     input:
#         pgs=config['output_dir'] + "/inputPGS/{score}.txt.gz"
#     output:
#         out=config['output_dir'] + "/inputPGS/chr_{score}.txt"
#     script:
#         "Scripts/pgs.format.R"


rule format_html:
    """Make a txt file from a html file. Params: headrow is the line number for the header and  bodyrow is the line number where the body text starts"""
    input:
        htmlf=config['pheno_html'],
    params:
        headrow=24,
        bodyrow=26
    output:
        out=config['output_dir'] + "/input_bb/pheno_table.txt"
    shell:
        "awk -v head={params.headrow} 'NR==head' {input.htmlf} | "
        " awk -F '[><]' '{{print $5\" \"$11\" \"$25\" \"$29 }}' "
        " > {output.out} ; "
        "awk -v  body={params.bodyrow} 'NR >=body' {input.htmlf} | "
        "awk -F '[><]' '{{print $5\" \"$11\" \"$17\" \"$29}}' "
        " >> {output.out} "

        
rule extract_samples_phenotype:
    """Given a phenotype description as coded in UKBB html file, get the sample IDs. Column f.eid is always extracted plus the column for particular phenotype. UDI field in html is NN-0.0 but in tab file is f.NN.0.0. I have selected the relevant dictionary ukbbDic or ld2_ukbbDic depending on what I need but can reapeat the rule, each  with each dictionary"""
    input:
        pheno=config['output_dir'] + "/input_bb/pheno_table.txt",
        eid=config['eid_pheno']
    params:
        code=lambda wildcards: ld2_ukbbDic[wildcards.ukbb_phenotype] 
    output:
        out=config['output_dir'] + "/input_bb/{ukbb_phenotype}.tab"
    shell:
        "col=$(awk '$0 ~/ {params.code}-0/{{print $2}}' {input.pheno} | "
        " awk '{{gsub(\"-\",\".\", $0); print $0}}') ; "
        "awk -v cols=\"f.eid,f.$col\" 'BEGIN {{"
        "FS=OFS=\"\\t\"; "
        "nc=split(cols, a, \",\")}} "
        "NR==1{{for (i=1; i<=NF; i++) hdr[$i]=i}} "
        "{{for (i=1; i<=nc; i++) "
        "if (a[i] in hdr) printf\"%s%s\", $hdr[a[i]], "
        "(i<nc?OFS:ORS)}}' {input.eid} > {output.out} "

rule checking_sample_files:
    """The sample files linked to the bgen files are per chromosome. Check if the samples are in the same order across bgen files, if so donwstream code can be simplified. If output file has a "0" line, all files are identical, if "1" they differ, "2" if trouble"""
    params:
        sample_dir=config['pheno_dir'] + "/samplefiles"
    output:
        out=config['output_dir'] + "/input_bb/diff_samplefiles"
    shell:
        "diff -q --from-file {params.sample_dir} ;"
        "echo $? > {output.out} "

        
rule comput_PGS_10:
    """Compute PGS for UKBB sub-sample. Sample files for bgen files are identical so I choose one.Code 1001 in params corresponds to British."""
    input:
        bgen=expand(config['geno_dir'] + "/ukb_imp_chr{chrom}_v3.bgen", chrom=[x+1 for x in range(22)]),
        pgs=config['output_dir'] + "/inputPGS/chr_PGS000010.txt",
        samples=config['output_dir'] + "/input_bb/ethnicity_samples.tab",
        sample_bgen=config['pheno_dir'] + "/samplefiles/ukb30931_imp_chr11_v3_s487296.sample"
    params:
        code=1001
    output:
        out=config['output_dir'] + "/ukbb_PGS/eth1001_PGS000010.txt"
    script:
        "Scripts/comput_PGS.R"

rule comput_PGS_all:
    """Compute PGS for UKBB sub-sample. I split PGS file and paralelise per chromosome up to 1000 (chunk parameter , same as b in breaks_chrom function) variants in each go."""
    input:
        bgen=config['geno_dir'] + "/ukb_imp_chr{chrom}_v3.bgen",
        pgs=config['output_dir'] + "/inputPGS/{score}.txt.gz",
        samples=lambda wildcards: config['output_dir'] + "/input_bb/" + pgs_ukbb[wildcards.score] + ".tab",
        sample_bgen=config['pheno_dir'] + "/samplefiles/ukb30931_imp_chr{chrom}_v3_s487296.sample"
    params:
        genome_build=lambda wildcards: built[wildcards.score],
        chunk=1000
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+"
    output:
        out=config['output_dir'] + "/ukbb_PGS/{score}/{score}.{chrom}.{breaks}.txt"
    script:
        "Scripts/comput_PGS.R"

rule merge_scores:
    """After computing scores by chrom chunks add sub-scores into one file. First awk command gets variable col which is the col number for the column labelled "score", colums 1:(score -1) are the same in all files. Second awk command adds up column score in all files and stores it in a temp file, keeping the first column to make sure rows are kept in order. Third  awk command takes one input file to print the first columns up to score and replaces the score column with the added scores stored in the temp file. Last swk command adds socre as column name to the last column. At the end the temp file is deleted."""
    input:
        scores=lambda wildcards: [config['output_dir'] + "/ukbb_PGS/" + wildcards.score + "/" + wildcards.score + ".{chrom}.{breaks}.txt".format(chrom=str(bk), breaks=str(v)) for  bk in break_chrom(n_chrom[wildcards.score]).keys() for v in break_chrom(n_chrom[wildcards.score])[bk]]
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+"
    output:
        scores=config['output_dir'] + "/ukbb_PGS/{score}/{score}.total.txt"
    shell:
        "col=$(awk 'FNR==1 {{for (i=1;i<=NF;i++) if ($i ~/score/) print i; exit}}' {input.scores[0]}) ;"
        "awk -v n=$col 'FNR==NR && NR >1 {{for (i=1;i<n;i++) same[FNR\",\"i]=$i; cnt=FNR}} "
        "{{x[$1] += $n}} END {{for( i=1; i<=cnt; i++) "
        "{{for (j=1;j<=NF;j++) if (j==1) print x[same[i\",\"j]],same[i\",\"j]  ;}}}}' "
        "{input.scores}  > {output.scores}1 ;"
        "awk -v n=$col 'FNR==NR {{a[$2]=$1;next}}{{$n=a[$1]; print ;}}' "
        "{output.scores}1 {input.scores[0]} > {output.scores} ;"
        "awk -v n=$col 'NR==1{{$n =\"score\"}}1' {output.scores} > {output.scores}1 ;"
        "mv {output.scores}1 {output.scores} ;"

rule test_scores:
    """Look at AUC or correlation between score and trait"""
    input:
        scores=expand(config['output_dir'] + "/ukbb_PGS/{score}/{score}.total.txt", score=pgs2test),
        pheno=config['pgs2ukbb'],
        script="Scripts/test.scores.R"
    params:
        pgs=pgs2test
    output:
        "Scripts/test.scores.R"
    script:
        "Scripts/RenderReport.R"


rule comput_PGS_ldpred2:
    """Compute PGS calculated using ldpred2 by Guillermo for UKBB. I split PGS file and paralelise per chromosome up to 5000 (chunk parameter , same as b in breaks_chrom function) variants in each go."""
    input:
        bgen=config['geno_dir'] + "/ukb_imp_chr{chrom}_v3.bgen",
        pgs=config['pgs_ldpred2'] + "/{ldname}",
        samples=lambda wildcards: config['output_dir'] + "/input_bb/" + pgsld2_ukbb[wildcards.ldname] + ".tab",
        sample_bgen=config['pheno_dir'] + "/samplefiles/ukb30931_imp_chr{chrom}_v3_s487296.sample"
    params:
        genome_build=37,
        chunk=1000,
        cols=["CHR19", "BP19", "REF", "ALT", "SNPID", "weight"]
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+"
    output:
        out=config['output_dir'] + "/ukbb_ldpred2/{ldname}/{ldname}.{chrom}.{breaks}.txt"
    script:
        "Scripts/comput_PGS.R"


rule merge_ld2scores:
    """After computing scores by chrom chunks add sub-scores into one file. I tried using awk but it couldnt cope with the number of files for some PGS, so I do the merging in python. Merging both partial PGS into 1 files and failed SNPs into another file, when appropiate. """
    input:
        scores=lambda wildcards: [config['output_dir'] + "/ukbb_ldpred2/" + wildcards.ldname + "/" + wildcards.ldname + ".{chrom}.{breaks}.txt".format(chrom=str(bk), breaks=str(v)) for bk 
in break_chrom(d=ld2_dic_counts[wildcards.ldname],b= 1000).keys() for v in break_chrom(d=ld2_dic_counts[wildcards.ldname], b=1000)[bk]]
    wildcard_constraints:
        chrom="\d+",
        breaks="\d+"
    output:
        scores=config['output_dir'] + "/ukbb_ldpred2/{ldname}/{ldname}.total.txt"
    script:
       "Scripts/merge_ld2scores.py"


rule test_ld2scores:
    """Look at AUC or correlation between score and trait"""
    input:
        scores=expand(config['output_dir'] + "/ukbb_ldpred2/{ldname}/{ldname}.total.txt", ldname=pgsld2_ukbb.keys()),
        ukbb_lds2=config['pgsldpred2ukbb'],
        script="Scripts/test.ldpred2_scores.R"
    output:
        "Scripts/test.ldpred2_scores.pdf"
    script:
        "Scripts/RenderReport.R"
        # "Scripts/test.ldpred2_scores.R"
        





        

# awk -v n=4 'FNR==NR {for (i=1;i<n;i++) same[FNR","i]=$i; cnt=FNR} {x[$1] += $n} END {for( i=1; i<=cnt; i++) {for (j=1;j<n;j++) if (j==1){ printf "%s,%s,", x[same[i","j]], same[i","j]} else {printf "%s\n", same[i","j] ;}}}' /mrc-bsu/scratch/ev250/ukbb_covid19/PGS000034/PGS000034.2.12.txt /mrc-bsu/scratch/ev250/ukbb_covid19/PGS000034/PGS000034.1.10.txt | head



## snakemake  -k -j 500 --cluster-config cpu.json --cluster "sbatch -A {cluster.account} -p {cluster.partition}  -c {cluster.cpus-per-task}   -t {cluster.time} --output {cluster.error} -J {cluster.job} "
